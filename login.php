<?php
$pagetitle = "Log in";
include("includes/pageheader.php");

if (empty($_SESSION["userid"])) {
?>

<div id="logindiv">

<div id="modeselection">
<span> <input type="radio" id="mode_login" name="mode" value="login" checked /> <label for="mode_login">Login</label> </span>
<span> <input type="radio" id="mode_register" name="mode" value="register" /> <label for="mode_register">Register</label> </span>
</div>

<div id="forms">
<!-- Only one of these is (should be) visible at any given time. This is controlled by the radio buttons above.

	 Note that, in a real website, HTML comments are never included in the page that's sent to the browser, as they have marginal use and only add bandwidth.
	 This also applies to indentation and almost all whitespace between tags, as well as JS and CSS comments.
	 Instead, you would have a development site which contains all the comments and properly formatted and human-readable HTML code, and a seperate
	   production site which is actually used by your users. You would have a script that removes all whitespace, comments, and probably every linebreak.
	 (Browsers are supposed to ignore whitespace and linebreaks anyway, which is why we have <br />.)
	 None of this applies to PHP, as it is only executed server-side and never sent to the client.
	-->
<div id="loginform">
<form action="process/login.php" method="POST">
	<label for="username">Username: </label> <input type="text" name="username" /> <br />
	<label for="password">Password: </label> <input type="password" name="password" /> <br />
	<input type="submit" value="Login" /> <input type="checkbox" name="remember" /><label for="remember">Remember me</label>
</form>
</div>

<div id="registerform" style="display:none;"> <!-- This one is invisible by default. -->
<form action="process/register.php" method="POST">
	<label for="username">Username: </label> <input type="text" name="username" /><br />
	<label for="password">Password: </label> <input type="password" name="password" /><br />
	<input type="submit" value="Register" />
</form>
</div>
</div>
</div>

<script type="text/javascript">
// This code powers the login/register mode. As soon as the document is loaded, we can look up elements.
// (Before this, getElementById and co return null.)
// Then we can add our event listeners to them which let us swap which div is visible.
document.addEventListener("DOMContentLoaded", function() {
	var loginradio = document.getElementById("mode_login");

	if (!loginradio.checked) {
		// The browser might have set a value that the user previously set. The default only applies when the user is completely new here.
		document.getElementById("loginform").style.display = "none";
		document.getElementById("registerform").style.display = null;
	}

	loginradio.onchange = function(event) {
		// These events only fire when a radio button is *enabled*, not changed, contrary to their name.
		// We don't have much to worry about that in this case, but if we had more than two states,
		//  it would be marginally slower than this because one of the divs gets "display: none" which it already had.
		// No sane use-case for this functionality would really suffer from this, but it still bugs me.
		// Fortunately, again we have nothing to worry about here because we have only two states.
		document.getElementById("loginform").style.display = null;
		document.getElementById("registerform").style.display = "none";
	};

	document.getElementById("mode_register").onchange = function(event) {
		document.getElementById("loginform").style.display = "none";
		document.getElementById("registerform").style.display = null;
	};
}, false);

</script>

<?php
} else {
	echo("You are already logged in.");
}
include("includes/pagefooter.php");
?>