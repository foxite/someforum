function editPost(element) {
	var postElement = element.parentElement.parentElement;
	var postId = postElement.getAttribute("data-post-id");
	var postContent = postElement.getElementsByClassName("postcontent")[0];
	var postEdit = postElement.getElementsByClassName("postedit")[0];
	var sourceElement = postElement.getElementsByClassName("markdownsource")[0];

	spawnEdit(postEdit, postContent, sourceElement, "post", postId);
}

function editComment(element) {
	var commentElement = element.parentElement.parentElement;
	var commentId = commentElement.parentElement.getAttribute("data-comment-id");
	var commentContent = commentElement.getElementsByClassName("commentcontent")[0];
	var commentEdit = commentElement.getElementsByClassName("commentedit")[0];
	var sourceElement = commentElement.getElementsByClassName("markdownsource")[0];

	spawnEdit(commentEdit, commentContent, sourceElement, "comment", commentId);
}

function spawnEdit(editContainer, contentElement, sourceElement, type, id) {
	var existingForms = editContainer.getElementsByTagName("form");
	if (existingForms.length >= 1) {
		// Don't make another form if one is present
		contentElement.style.display = "none";
		existingForms[0].style.display = null;
	} else {
		var editBase = document.getElementById("edit-base");
		var newEdit = editBase.cloneNode(true);
		newEdit.id = "edit-" + type + id;
		newEdit.setAttribute("data-entitytype", type);
		newEdit.setAttribute("data-entityid", id);
		newEdit.getElementsByTagName("textarea")[0].value = sourceElement.innerText;
		editContainer.appendChild(newEdit);

		var inputTags = newEdit.getElementsByTagName("input");
		for (var i = 0; i < inputTags.length; i++) {
			if (inputTags[i].name == "type") {
				inputTags[i].value = type;
			}
			if (inputTags[i].name == "id") {
				inputTags[i].value = id;
			}
		}

		contentElement.style.display = "none";
		newEdit.style.display = null;
	}
}

function replyComment(element) {
	var commentElement = element.parentElement.parentElement;
	if (commentElement.getElementsByClassName("commentreplybox")[0].childElementCount == 0) {
		var reply = document.getElementById("reply-base").cloneNode(true);
		var commentId = commentElement.getAttribute("data-comment-id");
		reply.id = "reply-" + commentId;
		commentElement.getElementsByClassName("commentreplybox")[0].appendChild(reply);
		reply.getElementsByTagName("textarea")[0].value = "";

		var inputs = reply.getElementsByTagName("input");
		for (var i = 0; i < inputs.length; i++) {
			if (inputs[i].name == "parent_comment") {
				inputs[i].value = commentId;
				break;
			}
		}

		reply.style.display = null;
	} else {
		document.getElementById("reply-" + element.parentElement.parentElement.parentElement.getAttribute("data-comment-id")).style.display = null;
	}
}

function closeReply(element) {
	element.parentElement.style.display = "none";
}

function closeEdit(element) {
	var inputTags = element.parentElement.getElementsByTagName("input");
	for (var i = 0; i < inputTags.length; i++) {
		element = inputTags[i];
		if (element.name == "type" && element.value == "post") {
			element.parentElement.parentElement.parentElement.parentElement.getElementsByTagName("main")[0].style.display = null;
			break;
		} else if (element.name == "type" && element.value == "comment") {
			element.parentElement.parentElement.parentElement.getElementsByClassName("commentcontent")[0].style.display = null;
			break;
		}
	}
	element.parentElement.style.display = "none";
}
