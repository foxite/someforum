const domain_base = "/edsa-login/";

function logout() {
	if (confirm("Are you sure you want to log out?")) {
		window.location.assign(domain_base + "process/logout.php");
	}
}
