// This js file requires that page.js has been loaded, as the constant domain_base is used.

function getPmUrl() {
	var outputElement = document.getElementById("templateUrl");
	var recipient = encodeURI(document.getElementById("recipient").value);
	var subject = encodeURI(document.getElementById("subject").value);
	var message = encodeURI(document.getElementById("message").value);

	// pathname is the url of the current page without existing get parameters or anchor names.
	// The page that uses this script (/composepm.php) does not have any <a>nchors at this time, so if any are added, make sure they are perserved here.
	outputElement.value = window.location.pathname + "?recipient=" + recipient + "&subject=" + subject + "&message=" + message;
	outputElement.style.display = null;
	outputElement.setAttribute("size", outputElement.value.length);
}

function markAsRead(element) {
	var messageElement = element.parentElement.parentElement;
	var messageId = messageElement.getAttribute("data-pmid");
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 204) {
			messageElement.classList.remove("unread");
			messageElement.classList.add("read");
		}
	};
	xhr.open("POST", domain_base + "process/pmmarkread.php", true);
	xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	xhr.send("id=" + messageId);
}
