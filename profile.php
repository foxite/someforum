<?php
// Don't include pageheader yet, as the title depends on the result of the query.
require_once("includes/notification.php");
require("includes/session.php");

if (empty($_SESSION["userid"])) {
	$pagetitle = "Error";
	include("includes/pageheader.php");
	echo("Error: You need to log in to view profiles.");
} else {
	$targetUserId = -1; // Dummy value: this will get set to something else.
	
	// If the ?user param is set, we use that, if it is valid.
	// Otherwise, we view the profile of the loggedin user.
	if (isset($_GET["user"]) && is_numeric($_GET["user"]) && intval($_GET["user"]) > 0) {
		$targetUserId = intval($_GET["user"]);
	} else {
		$targetUserId = $_SESSION["userid"];
	}

	require_once("includes/dbconnect.php");

	$sql = "SELECT u.username, u.registered, u.fav_fruit, f.variety AS fruitvariety, ft.name AS fruitname FROM users AS u
	LEFT JOIN fruit AS f ON (u.fav_fruit = f.id) -- Relational table: fruit.fruit_type_id -> fruit_types.id
	LEFT JOIN fruit_types AS ft ON (f.fruit_type_id = ft.id) -- Relational table: fruit.fruit_type_id -> fruit_types.id
	WHERE u.id = {$targetUserId} -- It is not necessary to escape $targetUserId because it is an integer
	LIMIT 1";

	$result = $conn->query($sql);

	if ($result->num_rows == 1) { // We're only looking for 1 user.
		$user = $result->fetch_assoc();

		$pagetitle = "View Profile - {$user['username']}";
		include("includes/pageheader.php");
		?>
		<h2>Profile: <?php echo($user["username"]); ?></h2>
		<div id="info">
		Registered on: <?php echo($user["registered"]); ?><br />
		Favourite fruit: <?php if (!empty($user["fav_fruit"])) {
			echo($user["fruitvariety"] . " " . $user["fruitname"]);
		} else {
			echo("None");
		} ?><br />
		</div>
		<?php
		if ($targetUserId == $_SESSION["userid"]) {
			?>
			<a href="editprofile.php">Edit your profile</a>
			<?php
		}

		?>
		<h2>Posts by <?php echo($user["username"]); ?></h2>
		<div id="userposts">
		<?php

		$startingFrom = 0;
		$count = 25;
		if (isset($_GET["from"]) && is_numeric($_GET["from"]) && intval($_GET["from"]) >= 0) {
			$startingFrom = intval($_GET["from"]);
		}
		if (isset($_GET["count"]) && is_numeric($_GET["count"]) && intval($_GET["count"]) >= 0 && intval($_GET["count"]) < 100) {
			$count = intval($_GET["count"]);
		}

		$sql = "SELECT t.id, t.author_id, t.title, t.posted, u.username FROM threads AS t
		LEFT JOIN users AS u ON (u.id = t.author_id)
		WHERE t.author_id = {$targetUserId}
		ORDER BY t.id DESC
		LIMIT {$count} OFFSET {$startingFrom}";

		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
			$notFirstRecord = false;
			while ($post = $result->fetch_assoc()) {
				if ($notFirstRecord) {
					echo("<hr />");
				} else {
					$notFirstRecord = true;
				}
				include("includes/postheader.php");
			}
		} else {
			echo("No posts have been made yet.");
		}

		?>
		</div>
		<?php
	} else {
		$pagetitle = "Error";
		include("includes/pageheader.php");
		echo("That profile was not found.");
	}
}

include("includes/pagefooter.php");
?>