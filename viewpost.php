<?php

if (isset($_GET["post"]) && is_numeric($_GET["post"]) && intval($_GET["post"]) > 0) {
	$postId = intval($_GET["post"]);

	$sql = "SELECT t.id, t.author_id, t.title, t.content, t.posted, t.edited, t.comment_count, t.markdown_source, u.username FROM threads AS t
	LEFT JOIN users AS u ON (u.id = t.author_id)
	WHERE t.id = {$postId}
	LIMIT 1";

	require_once("includes/dbconnect.php");
	$result = $conn->query($sql);

	if ($result->num_rows == 1) {
		$post = $result->fetch_assoc();

		$pagetitle = $post["title"];
		include("includes/pageheader-prehead.php");
		?>
		<link rel="stylesheet" href="css/post.css" />
		<link rel="stylesheet" href="css/markdown.css" />
		<script type="text/javascript" src="js/post.js"></script>
		<?php
		include("includes/pageheader-posthead.php");
		include("includes/posthtml.php");
		// postheader.php uses the $post variable.
		include("includes/postheader.php");
		
		?><div class="postbody">
			<main class="postcontent"><?php
			if (!empty($post["content"])) {
				echo($post["content"]);
			}
		  ?></main>
		  	<div class="markdownsource"><?php echo(htmlspecialchars($post["markdown_source"])); ?></div>
			<div class="postedit"></div>
		</div><?php
		$postVisible = true;
		include("includes/postfooter.php");
		?>
		<hr />
		<h4>Comments</h4>
		<div id="commentform">
		<h5>Post new comment</h5>
		<form id="reply-comment" action="process/newcomment.php" method="POST">
		<input type="hidden" name="parent_post" value="<?php echo($postId); ?>" />
		<input type="hidden" name="parent_comment" value="" />
		<textarea name="comment" cols="50" rows="6" maxlength="2000"></textarea><br />
		<input type="submit" value="Submit" />
		</form>
		</div>
		<?php
		include("includes/commenttree.php");
	} else {
		$pagetitle = "Error";
		include("includes/pageheader.php");
		echo("That post could not be found.");
	}
} else {
	$pagetitle = "Error";
	include("includes/pageheader.php");
	echo("No post has been specified.");
}

include("includes/pagefooter.php");
?>