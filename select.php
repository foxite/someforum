<?php

// Run the connection script. It creates $conn, which we can use after the script has executed.
require_once("includes/dbconnect.php");

// Make the SQL query.
$fruit = $conn->real_escape_string("Apple"); // Escape SQL input. No user input is taken, but do it anyway, for good form.
$sql = "SELECT fruit_types.name, fruit.variety FROM fruit -- Select only the columns we use
LEFT JOIN fruit_types ON (fruit.fruit_type_id = fruit_types.id) -- Relational table: fruit.fruit_type_id -> fruit_types.id
WHERE name = '{$fruit}'"; // Only select fruits we ask for. VSC seems to recognize "name", but we don't use it as a keyword. Perhaps this might cause trouble later on.

$result = $conn->query($sql);

if ($result->num_rows > 0) {
	while ($row = $result->fetch_assoc()) { // This is like a C# foreach loop, but different.
		echo("{$row['name']} {$row['variety']}<br />"); // We can access `fruit_types`.`name` as well as `fruit`.`variety` because of left join
	}
} else {
	// Note that if we're searching by name, we can make it slightly faster for invalid names, by checking if the name exists in `fruit_types` before searching in `fruit`.
	// I'll leave that as an excersie to the reader :P
	echo("No rows found");
}

?>