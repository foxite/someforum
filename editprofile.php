<?php

require_once("includes/notification.php");
if (session_status() === PHP_SESSION_NONE) {
	session_start();
}

if (empty($_SESSION["userid"])) {
	$pagetitle = "Error";
	include("includes/pageheader.php");
	echo("Error: You need to log in to edit your profile.");
} else {
	require_once("includes/dbconnect.php");

	if (isset($_POST["save"])) {
		// Update record
		$sql = "UPDATE users
		SET ";
		$notFirstColumn = false;
		if (isset($_POST["favfruit"]) && is_numeric($_POST["favfruit"]) && intval($_POST["favfruit"]) >= 0) {
			$fruitval = intval($_POST["favfruit"]);
			if ($fruitval === 0) {
				$fruitval = "NULL";
			}

			// This is unnecessary right now, but in the future we'll have more fields, and this way we can easily copypaste this block.
			// If we do that, we can remove this if/else from the first (this) block.
			if ($notFirstColumn) {
				$sql .= ", ";
			} else {
				$notFirstColumn = true;
			}
			
			$sql .= "fav_fruit = {$fruitval}";
		}
		$sql .= "
		WHERE id = {$_SESSION['userid']}";

		$result = $conn->query($sql);
		if ($result === true) {
			addNotifToSession(new Notification("white", "limegreen", "Your profile has been updated", null, Notification::NotifShowOnce));
		} else {
			addNotifToSession(new Notification("white", "red", "There was an error updating your profile", null, Notification::NotifShowOnce));
		}
	}

	$sql = "SELECT u.username, u.registered, u.fav_fruit FROM users u
	LEFT JOIN fruit f ON (u.fav_fruit = f.id)
	LEFT JOIN fruit_types ft ON (f.fruit_type_id = ft.id)
	WHERE u.id = {$_SESSION["userid"]}
	LIMIT 1";

	$result = $conn->query($sql);

	if ($result->num_rows == 1) { // We're only looking for 1 user.
		$user = $result->fetch_assoc();

		$pagetitle = "Edit profile";
		include("includes/pageheader.php");
?>
<h3>Editing profile: <?php echo($user["username"]); ?></h3>
<div id="info">
	<form action="editprofile.php" method="POST">
		<input type="hidden" name="save" value="1" />
		Registered on: <?php echo($user["registered"]); ?><br />
		<label for="favfruit">Favourite fruit:</label> <select name="favfruit">
			<option value="0"></option>
			<?php
			$sql = "SELECT fruit.id, fruit_types.name, fruit.variety FROM fruit
			LEFT JOIN fruit_types ON (fruit.fruit_type_id = fruit_types.id)
			ORDER BY name";
			
			$fruitResult = $conn->query($sql);
			
			if ($fruitResult->num_rows > 0) {
				while ($row = $fruitResult->fetch_assoc()) {
					if ($user["fav_fruit"] == $row["id"]) {
						echo("<option value='{$row["id"]}' selected>{$row["variety"]} {$row["name"]}</option>");
					} else {
						echo("<option value='{$row["id"]}'>{$row["variety"]} {$row["name"]}</option>");
					}
				}
			} else {
				echo("No rows found");
			}
			?>
		</select>
		<br />
		<input type="submit" value="Submit">
	</form>
</div>
<?php
	} else {
		// Should never happen
		echo("That profile was not found. Please contact an administrator to resolve this issue.");
	}
}

include("includes/pagefooter.php");
?>