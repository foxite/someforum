<?php

if (isset($_GET["comment"]) && is_numeric($_GET["comment"]) && intval($_GET["comment"]) > 0) {
	$commentId = $_GET["comment"];

	$sql = "SELECT c.parent_post, u.username FROM comments AS c
	LEFT JOIN users AS u ON (u.id = c.author_id)
	WHERE c.id = {$commentId}
	LIMIT 1";

	require_once("includes/dbconnect.php");
	$result = $conn->query($sql);
	if ($result->num_rows == 1) {
		$comment = $result->fetch_assoc();
		$postId = $comment["parent_post"]; // Keep in variable as it is used by commenttree.php

		$sql = "SELECT t.id, t.author_id, t.title, t.posted, t.edited, t.comment_count, u.username FROM threads AS t
		LEFT JOIN users AS u ON (u.id = t.author_id)
		WHERE t.id = {$postId}
		LIMIT 1";

		$post = $conn->query($sql)->fetch_assoc();
		// Assume that parent_post is valid and thus $result contains 1 row

		$pagetitle = $comment["username"] . " comments on " . $post["title"];
		include("includes/pageheader-prehead.php");
		?>
		<link rel="stylesheet" href="css/post.css" />
		<link rel="stylesheet" href="css/markdown.css" />
		<script type="text/javascript" src="js/post.js"></script>
		<?php
		include("includes/pageheader-posthead.php");
		include("includes/posthtml.php");
		// postheader.php uses the $post variable.
		include("includes/postheader.php");
		$postVisible = false;
		include("includes/postfooter.php");
		?>
		<hr />
		<p>
		<h4>Comments</h4>
		<a href="viewpost.php?post=<?php echo($post["id"]); ?>" class="smallish">&lt;- Go back to the main comments page</a>
		</p>
		<?php
		include("includes/commenttree.php");
	} else {
		$pagetitle = "Error";
		include("includes/pageheader.php");
		echo("That comment could not be found.");
	}
} else {
	$pagetitle = "Error";
	include("includes/pageheader.php");
	echo("No comment has been specified.");
}

include("includes/pagefooter.php");
?>