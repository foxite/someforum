<?php
// This include file uses the object $comment and expects it to contain "id", "username", "author_id", "posted", "edited" (may be null), "markdown_source", and "content". It uses the object $post and expects it to contain "id" and "author_id". It uses the session.
// Proper display of this includes file *requires* that you append "</div></div></div>" behind it, this is to allow placement of comment children without using two includes.
?><div data-comment-id="<?php echo($comment["id"]); ?>" class="comment"><a name="<?php echo($comment["id"]); ?>"></a>
<input id="comment-collapse-<?php echo($comment["id"]); ?>" type="checkbox" class="ccbcontrol" checked />
<div class="commentheader"><label for="comment-collapse-<?php echo($comment["id"]); ?>"><span class="ccbshow">[+]</span><span class="ccbhide">[-]</span></label>
<a href="profile.php?user=<?php echo($comment["author_id"]) ?>" class="commentauthor"><?php echo($comment["username"]); ?></a>
<a href="viewpost.php?post=<?php echo($post["id"]) ?>#<?php echo($comment["id"]); ?>"><time class="commentposted"><?php echo($comment["posted"]); ?></time></a>
<?php if (!empty($comment["edited"])) { ?><sup class="editedstamp">edited<time class="editdatetime"> <?php echo($comment["edited"]); ?></time></sup><?php } ?></div>
<div class="ccbcontent">
<div class="commentcontent"><?php echo($comment["content"]); ?></div>
<div class="commentoptions"><?php
if (!empty($_SESSION["userid"])) {
	?><a class="button" onclick="replyComment(this)">Reply</a> <?php
	if ($_SESSION["userid"] == $post["author_id"]) {
		?><a class="button" onclick="editComment(this)">Edit</a><?php
	}
}
?></div>
<div class="markdownsource"><?php echo(htmlspecialchars($comment["markdown_source"])); ?></div>
<div class="commentedit"></div>
<div class="commentreplybox"></div>
<div class="commentchildren">