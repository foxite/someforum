<?php
// This include file uses: $count, $from, and optionally, $targetUserId. It requires dbconnect.php once.

require_once("dbconnect.php");

$sql = "SELECT t.id, t.author_id, t.title, t.posted, t.edited, t.comment_count, u.username FROM threads AS t
LEFT JOIN users AS u ON (u.id = t.author_id)";
if (!empty($targetUserId)) {
	$sql .= "WHERE t.author_id = {$targetUserId}";
}
$sql .= "ORDER BY t.id DESC
LIMIT {$count} OFFSET {$startingFrom}";

$result = $conn->query($sql);

$postVisible = false;
if ($result->num_rows > 0) {
	$notFirstRecord = false;
	while ($post = $result->fetch_assoc()) {
		if ($notFirstRecord) {
			echo("<hr />");
		} else {
			$notFirstRecord = true;
		}
		include("includes/postheader.php");
		include("includes/postfooter.php");
	}
} else {
	echo("No posts have been made yet.");
}

?>