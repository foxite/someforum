<?php
// This includes file uses $postId, and optionally, $commentId, and/or $displayCancel.
// It requires config.php once.
// If you change this, be sure to change the version in viewpost.php which does not have the cancel <button>.

require_once("config.php");
?><form id="reply-base" style="display:none;" action="<?php echo(DOMAIN_BASE); ?>process/newcomment.php" method="POST">
<input type="hidden" name="parent_post" value="<?php echo($postId); ?>" />
<input type="hidden" name="parent_comment" value="" />
<textarea name="comment" cols="50" rows="6" maxlength="2000"></textarea><br />
<input type="submit" value="Submit" /> <button type="button" onclick="closeReply(this)">Cancel</button>
</form>