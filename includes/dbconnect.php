<?php
// This include file writes to $conn. It requires config.php once. It can die.

require_once("config.php");

$conn = new mysqli(DB_SERV, DB_USER, DB_PASS, DB_NAME);
if ($conn->connect_error) {
	die("Connection failed: " . $conn->connect_error);
}

?>