<?php
// This include file defines the class Notification and the function addNotifToSession($notif).

class Notification {
	const NotifShowOnce = 0;
	const NotifNewMessage = 1;

	public $textColor;
	public $backgroundColor;
	public $message;
	// The linked page for this Notification.
	public $link;
	// One of the constants on top of the class.
	public $type;

	// If $link_ is null, no <a> tag will be produced for this Notification.
	function __construct($textColor_, $backgroundColor_, $message_, $link_, $type_) {
		$this->textColor = $textColor_;
		$this->backgroundColor = $backgroundColor_;
		$this->message = $message_;
		$this->link = $link_;
		$this->type = $type_;
	}

	function toHtml() {
		$ret = '<div class="notification" style="color:' . $this->textColor . ';background-color:' . $this->backgroundColor . '">' . $this->message . '</div>';
		if (!empty($this->link)) {
			$ret = '<a href="' . $this->link . '">' . $ret . '</a>';
		}
		return $ret;
	}
}

function addNotifToSession($notif) {
	if (!isset($_SESSION["notifs"])) {
		$_SESSION["notifs"] = array();
	}
	$_SESSION["notifs"][] = $notif;
}

?>