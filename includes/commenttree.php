<?php
// This includes file uses $isPost, and optionally, $commentId.
// It defines the class Comment.
// It requires dbconnect.php once.

// A Comment is be a class consisting of the SQL row that was originally obtained, an integer of how many parents it has, and an array of all Comment objects that have that comment as its parent. This facilitates the desired tree-like structure.
// All comments on a post without a parent will be contained in an array held in the PHP script.
// Furthermore, a reference to all Comment objects being handled will be stored by their ID in an array to accelerate searching.
// 
// To setup this tree we select all comments in the desired post (even if looking for a specific comment), and then add all comments with no parent directly into the tree array, and all comments with a parent will be added into the array of the parent. To find this array, we would need to search the entire tree every time, but this will be accelerated by having an associative with *all* comments by their ID, including comments that have a parent comment.
// Recursion is limited to 15 comments. If more comments exist below this level, a link to that specific comment will be placed (which will display its own tree, limited to 15 levels of recursion, and so on), and children of that comment will not be added to the tree.
// 
// To display the data we use a recursive function defined in Comment, which `include`s comment.php with the SQL row that it carries, and then calls itself on all comments in its array.

class Comment {
	public $dbRow;
	public $numParents;
	public $children;

	public function __construct($dbRow) {
		$this->dbRow = $dbRow;
		$this->children = array();
		$this->numParents = 0; // It will be set by calling addChild and passing this Comment.
	}

	public function addChild($comment) {
		$comment->numParents = $this->numParents + 1;
		$this->children[] = $comment;
	}

	public function displayHtml() {
		$post = array("id" => $this->dbRow["parent_post"], "author_id" => $this->dbRow["author_id"]);
		$comment = $this->dbRow;
		$displayCancel = true;
		include("comment.php");
		if (sizeof($this->children) > 0) {
			if ($this->numParents < 14) {
				for ($i = 0; $i < sizeof($this->children); $i++) {
					$this->children[$i]->displayHtml();
				}
			} else {
				?><a href="viewcomment.php?comment=<?php echo($this->dbRow["parent_comment"]); ?>" class="smallish">More comments...</a><?php
			}
		}
		echo("</div></div></div>");
	}
}

$sql = "SELECT c.id, c.author_id, c.parent_comment, c.parent_post, c.content, c.edited, c.posted, c.markdown_source, u.username FROM comments AS c
LEFT JOIN users AS u ON (c.author_id = u.id)
WHERE parent_post = {$postId}
ORDER BY c.id ASC
LIMIT 100";

require_once("dbconnect.php");

$result = $conn->query($sql);
if ($result->num_rows > 0) {
	$commentTree = array();
	$allComments = array();

	include("commentreply.php");

	// Set up comment tree
	if (isset($commentId)) {
		while ($row = $result->fetch_assoc()) {
			// TODO figure out a way to skip processing comments that are not part of this comment tree
			// We could try only selecting rows after and including the comment we're looking for, but that doesn't entirely cut out irrelevant comments.
			$comment = new Comment($row);
			$allComments[$row["id"]] = $comment;
			if ($row["parent_comment"] == $commentId) {
				// We can assume that its parent exists in $allComments, because the comments are sorted by ID, and it is impossible for a comment to have a lower ID as its parent.
				// However, it is possible that the comment has no parent, in which case it will be placed directly in the $commentTree.
				$commentTree[] = $comment;
			} else if ($row["parent_comment"] != "") {
				$allComments[$row["parent_comment"]]->addChild($comment);
			}
		}
	} else {
		while ($row = $result->fetch_assoc()) {
			$comment = new Comment($row);
			$allComments[$row["id"]] = $comment;
			if ($row["parent_comment"] == "") {
				$commentTree[] = $comment;
			} else {
				$allComments[$row["parent_comment"]]->addChild($comment);
			}
		}
	}

	// Display data
	?><section id="comments"><?php
	for ($i = 0; $i < sizeof($commentTree); $i++) {
		$commentTree[$i]->displayHtml();
	}
	?></section><?php
} else {
	if (isset($commentId)) {
		echo("That comment does not exist.");
	} else {
		echo("There are no comments here.");
	}
}

?>