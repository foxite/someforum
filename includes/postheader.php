<?php
// This include file requires config.php once. It uses the object $post, and expects it to contain "id", "title", "author_id", "username", and "posted".

require_once("config.php");
?><article id="post-<?php echo($post["id"]); ?>" data-post-id="<?php echo($post["id"]); ?>">
<div class="postheader">
<h3><a href="<?php echo(DOMAIN_BASE); ?>viewpost.php?post=<?php echo($post["id"]); ?>"><?php echo($post["title"]) ?></a></h3>
By <a href="<?php echo(DOMAIN_BASE); ?>profile.php?user=<?php echo($post["author_id"]); ?>"><?php echo($post["username"]) ?></a> on <a href="viewpost.php?post=<?php echo($post["id"]) ?>#<?php echo($post["id"]); ?>"><time class="postposted"><?php echo($post["posted"]); ?></time></a>
<?php if (!empty($post["edited"])) { ?><sup class="postedited" title="Last edited <?php echo($post["edited"]); ?>">edited</sup><?php } ?></div>