<?php
// This include file defines the class NavButton. It requires config.php once. It requires dbconnect once. It declares $navbuttons.
?></head>
<body>
<div id="container">
<header><h1>Someforum</h1></header>
<nav><?php

require_once("config.php");

class NavButton {
	const DisplayAlways = 0;
	const DisplayLoggedOut = 1;
	const DisplayLoggedIn = 2;
	
	public $text;
	public $link;
	public $onclick;
	public $display;
	public $cssClasses;

	function __construct($text, $link, $onclick, $display, $cssClasses) {
		$this->text = $text;
		$this->link = $link;
		$this->onclick = $onclick;
		$this->display = $display;
		$this->cssClasses = $cssClasses;

		if (substr($_SERVER["PHP_SELF"], strlen(DOMAIN_BASE)) == $this->link) { 
			$this->cssClasses .= " selected";
		}
	}

	function toHtml() {
		if ($this->display == NavButton::DisplayAlways ||
		  (($this->display == NavButton::DisplayLoggedOut) &&  empty($_SESSION["userid"])) ||
		  (($this->display == NavButton::DisplayLoggedIn ) && !empty($_SESSION["userid"]))) {
			if (!empty($this->link)) {
				return $this->linkToHtml();
			} else if (!empty($this->onclick)) {
				return $this->onClickToHtml();
			}
		}
	}

	private function onClickToHtml() {
		return "<a class='{$this->cssClasses}' onclick='{$this->onclick}'>{$this->text}</a>";
	}

	private function linkToHtml() {
		return "<a class='{$this->cssClasses}' href='{$this->link}'>{$this->text}</a>";
	}
}

$unreadMessages = 0;
if (isset($_SESSION["userid"])) {
	require_once("dbconnect.php");
	$unreadMessages = $conn->query("SELECT COUNT(id) FROM messages WHERE recipient_id = {$_SESSION["userid"]} AND read_by_recipient = 0")->fetch_row()[0];
}

$navbuttons = array(new NavButton("Home", "index.php", null, NavButton::DisplayAlways, null),
					new NavButton("Members", "memberlist.php", null, NavButton::DisplayLoggedIn, null),
					new NavButton("New post", "newpost.php", null, NavButton::DisplayLoggedIn, null),
					new NavButton("Log in", "login.php", null, NavButton::DisplayLoggedOut, "right"),
					new NavButton("Log out", null, "logout()", NavButton::DisplayLoggedIn, "right"),
					new NavButton("Profile", "profile.php", null, NavButton::DisplayLoggedIn, "right"),
					new NavButton("Messages" . ($unreadMessages ? (" (" . $unreadMessages . ")") : ""), "messages.php", null, NavButton::DisplayLoggedIn, "right" . ($unreadMessages > 0 ? " unreadMsgs" : "")));

foreach ($navbuttons as $button) {
	echo($button->toHtml());
}

?></nav>
<div id="notifications">
<?php
// Display notifications
if (!empty($_SESSION["notifs"])) {
	foreach ($_SESSION["notifs"] as $idx => $notif) {
		echo($notif->toHtml());
		if ($notif->type == Notification::NotifShowOnce) {
			unset($_SESSION["notifs"][$idx]);
		}
	}
}
?>
</div>
<hr />
<div id="content">