<?php
// This include file requires session.php and config.php once. It uses $pagetitle if it is set.

require_once("session.php");
require_once("config.php");
?>
<!DOCTYPE html>
<html>
<head>
	<title><?php if (isset($pagetitle)) { echo($pagetitle . " | "); } ?>Example2</title>

	<script type="text/javascript" src="<?php echo(DOMAIN_BASE); ?>js/page.js"></script>
	<link rel="stylesheet" href="<?php echo(DOMAIN_BASE); ?>css/page.css" />