<?php
// This include file requires notification.php once. It starts the session if it is not active.

require_once("notification.php");
if (session_status() === PHP_SESSION_NONE) {
	session_start();
}
?>