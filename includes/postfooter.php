<?php
// This include file uses the object $post and expects it to contain "id", "author_id", and "comment_count". It uses $postVisible. It uses the session.
?><div class="postfooter"><a class="small"><?php echo($post["comment_count"]); ?> comment<?php if (intval($post["comment_count"]) != 1) { echo("s"); } ?></a> <?php
if ($postVisible == true && !empty($_SESSION["userid"]) && $_SESSION["userid"] == $post["author_id"]) {
	?><a class="button small" onclick="editPost(this)">Edit</a><?php
}
?></div></article>