<?php

require_once("includes/session.php");

if (empty($_SESSION["userid"])) {
	$pagetitle = "Error";
	include("includes/pageheader.php");

	echo("You need to log in to post.");
} else {
	$pagetitle = "New post";
	include("includes/pageheader.php");
	?>

<h2>New post</h2>

<form action="process/newpost.php" method="POST">
<label for="title">Post title:</label><br /><input type="text" name="title" size="80" maxlength="128" required /><br />
<label for="content">Content:</label><br /><textarea name="content" rows="10" cols="80" maxlength="2000"></textarea><br />
<input type="submit" />
</form>

<?php
}
include("includes/pagefooter.php");
?>