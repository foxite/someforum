<?php

// Check if we have the data we need. Otherwise, give a HTTP 500 (Bad request).
if (isset($_POST["username"]) && isset($_POST["password"])) {
	// Enforce username rules.
	// Password rule enforcement is only necessary on the client side because they are never displayed, and they only
	//  protect the *user* (not us) from setting a terrible password that can be bruteforced within 10 minutes.
	// If the user is smart enough to bypass the login form and the password rules, we can't be held responsible for their digital identity theft.
	// Hopefully. Maybe we should get a laywer to think about that.

	$registrationValid = true;
	$usernameBlacklist = array("admin", "administrator", "mod", "moderator", "system", "account", "you", "guest", "bot", "blocked", "banned", "deleted", $_POST["password"]);
	$usernameBannedCharacters = array("<", ">", "\\", "\'", "\t", "\n", "\r", "\"", "{", "}", "(", ")", "[", "]", "`", "~", "!", "@", "#", "$", "%", "^", "&", "*");

	// Trim leading and trailing whitespace
	$username = trim($_POST["username"]);

	// Make sure username is not empty after trimming
	if (empty($username)) {
		http_response_code(400);
		die("That username is empty.");
	}

	// Detect non-ASCII characters: from https://stackoverflow.com/a/6497946/3141917
	if (preg_match('/[^\x20-\x7f]/', $username)) {
		http_response_code(400);
		die("That username is invalid. Please choose a different one.");
	}

	// Check for banned usernames
	if (in_array(strtolower($username), $usernameBlacklist)) {
		http_response_code(400);
		die("That username is not allowed. Please choose a different one.");
	}

	// Check for banned characters
	$rejectedCharacters = "";
	foreach ($usernameBannedCharacters as $character) {
		if (strpos($username, $character) !== false) {
			$registrationValid = false;
			$rejectedCharacters .= " " . htmlspecialchars($character);
		}
	}
	if (!empty($rejectedCharacters)) {
		http_response_code(400);
		die("Your username contains invalid characters: " . $rejectedCharacters);
	}

	if (sizeof($username) > 20) {
		http_response_code(400);
		die("That username is too long.");
	}

	// Check if password is not empty
	if (empty($_POST["password"])) {
		http_response_code(400);
		die("Your password cannot be empty.");
	}
	
	// Connect to the DB. This script creates the mysqli connection $conn.
	require_once("../includes/dbconnect.php");
	
	// Look for existing users with this name.
	$sqlUsername = $conn->real_escape_string($username); // It is important to sanitize strings passed into SQL queries.
	$sql = "SELECT * FROM `users`
	WHERE `username`='{$sqlUsername}'
	LIMIT 1"; // Do not keep looking after finding a record, because we only need to know if there are 0.
	$result = $conn->query($sql);

	// If we didn't find anything, that means the username is not taken.
	// Note that in large websites, multiple database servers are running and will sync up at intervals, which means it would be possible
	//  for someone (or multiple people by accident) to create two users with the same username by connecting to different database servers.
	// This would cause all kinds of problems, but that's a topic that we don't have to worry about here.
	if ($result->num_rows == 0) {
		// Hash the password for database storage. password_verify is used to check the password against this.
		// BCrypt is good because it is very slow, by design, which makes it very hard to bruteforce the hashed passwords after database theft.
		// A properly salted hash will already make rainbow table attacks useless, although they don't make bruteforcing harder, because
		//  the salt is just stored alongside the hash.
		// A slow hashing algorithm increases the average time for a bruteforce attack.
		// However, nothing will protect users who just set their password to "12345". For that we have password rules.
		$hashedPw = password_hash($_POST["password"], PASSWORD_BCRYPT);
		$sql = "INSERT INTO users (username, password, registered)
		VALUES ('{$sqlUsername}', '{$hashedPw}', now())";

		if ($conn->query($sql) === true) {
			// The query was successful: Congrats, your account is made.
			require_once("../includes/notification.php");
			
			session_start();

			$_SESSION["userid"] = $conn->insert_id;
			
			addNotifToSession(new Notification("white", "limegreen", "Your account was created and you are now logged in", null, Notification::NotifShowOnce));
			http_response_code(200);
			header("X-New-User-Id: " . $conn->insert_id);
			header("Location: ../index.php");
		} else {
			// I don't know what might have caused this.
			// It's actually a pretty bad idea to just echo the error message, as someone might have been trying to cause errors on purpose
			//  and this would give them valuable insight to how your code works. Basically, hacking becomes easier.
			// But this is just a school project and I'm not going to set up a proper notification system for myself.
			// (That's what you should do - send a notification to engineers that requires their immediate attention.)
			http_response_code(500);
			echo("An error occurred.<br />" . $conn->error);
		}
	} else {
		http_response_code(409);
		echo("That username is already taken.");
	}
} else {
	http_response_code(400); // Bad request
}

?>