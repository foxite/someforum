<?php

require("../includes/session.php");

if (!empty($_SESSION["userid"])) {
	if (isset($_POST["type"]) && ($_POST["type"] == "comment" || $_POST["type"] == "post") && isset($_POST["id"]) && is_numeric($_POST["id"]) && intval($_POST["id"]) > 0 && isset($_POST["content"])) {
		$targetIsPost = $_POST["type"] == "post";
		$targetId = intval($_POST["id"]);
		$targetTable = $targetIsPost ? "threads" : "comments";

		// Check if post exists
		$sql = "SELECT id"; // We don't use the id, but we need at least 1 column, and parent_post may not be added.

		if (!$targetIsPost) {
			$sql .= ", parent_post";
		}

		$sql .= " FROM {$targetTable}
		WHERE id = {$targetId}
		LIMIT 1";

		require_once("../includes/dbconnect.php");
		$result = $conn->query($sql);
		if ($result->num_rows == 1) {
			require_once("../includes/parsedown.php");
			$parsedown = new Parsedown();
			$parsedown->setSafeMode(true);
			$content = $conn->real_escape_string($parsedown->text($_POST["content"]));
			$markdownSource = $conn->real_escape_string($_POST["content"]);

			$sql = "UPDATE {$targetTable}
			SET content = '{$content}', markdown_source = '{$markdownSource}'
			WHERE id = {$targetId}";

			if ($conn->query($sql)) {
				if ($targetIsPost) {
					addNotifToSession(new Notification("white", "limegreen", "Your post has been edited", null, Notification::NotifShowOnce));
				} else {
					addNotifToSession(new Notification("white", "limegreen", "Your comment has been edited", null, Notification::NotifShowOnce));
				}
			} else {
				if ($targetIsPost) {
					addNotifToSession(new Notification("white", "red", "Your post could not be edited", null, Notification::NotifShowOnce));
				} else {
					addNotifToSession(new Notification("white", "red", "Your comment could not be edited", null, Notification::NotifShowOnce));
				}
			}

			$targetPost = -1;
			if ($targetIsPost) {
				$targetPost = $targetId;
			} else {
				$row = $result->fetch_assoc();
				$targetPost = $row["parent_post"];
			}

			$locationHeader = "Location: ../viewpost.php?post=" . $targetPost;
			if (!$targetIsPost) {
				$locationHeader .= "#{$targetId}";
			}
			http_response_code(200);
			header($locationHeader);
		} else {
			http_response_code(400);
		}
	} else {
		http_response_code(400);
	}
} else {
	http_response_code(403);
}

?>