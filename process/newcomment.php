<?php

require_once("../includes/config.php");
require("../includes/session.php");
if (!empty($_SESSION["userid"])) {
	// Each numeric parameter must be checked for isset (or !empty), is_numeric, AND intval, because of the unpredictable behaviour of intval for strings
	//  that are not actually integers.
	// Furthermore, calling is_numeric on a variable in $_POST that has not been sent will print a notice.
	// See the user notes on https://secure.php.net/manual/en/function.is-numeric.php
	if (!empty($_POST["parent_post"]) && is_numeric($_POST["parent_post"]) && intval($_POST["parent_post"]) > 0
	&& ( empty($_POST["parent_comment"]) || (!empty($_POST["parent_comment"]) && is_numeric($_POST["parent_comment"]) && intval($_POST["parent_comment"]) > 0)) // parent_comment is empty, or (it is not empty and it is an int > 0)
	&&  !empty($_POST["comment"]) && sizeof(strval($_POST["comment"])) <= 2000) {
		require_once("../includes/dbconnect.php");
		
		$parentPost = intval($_POST["parent_post"]);
		$parentComment = !empty($_POST["parent_comment"]) ? (intval($_POST["parent_comment"])) : "NULL";

		// Sanitize text: Escape html using Parsedown and then sql escape.
		require_once("../includes/parsedown.php");
		$parsedown = new Parsedown();
		$parsedown->setSafeMode(true);
		$comment = $conn->real_escape_string($parsedown->text($_POST["comment"]));
		$markdownSource = $conn->real_escape_string($_POST["comment"]);

		$sql = "INSERT INTO comments (author_id, parent_comment, parent_post, content, posted, markdown_source)
		VALUES ({$_SESSION['userid']}, {$parentComment}, {$parentPost}, '{$comment}', now(), '{$markdownSource}')";

		if ($conn->query($sql) == true) {
			$sql = "UPDATE threads AS t
			SET t.comment_count = t.comment_count + 1
			WHERE t.id = {$parentPost}";
			$conn->query($sql);

			addNotifToSession(new Notification("white", "limegreen", "Your comment has been made", null, Notification::NotifShowOnce));
			http_response_code(200);
			header("Location: " . DOMAIN_BASE . "viewpost.php?post=" . intval($_POST["parent_post"]) . "#{$conn->insert_id}");
		} else {
			addNotifToSession(new Notification("white", "red", "Your comment could not be made", null, Notification::NotifShowOnce));
			http_response_code(500);
			header("Location: " . DOMAIN_BASE . "viewpost.php?post=" . intval($_POST["parent_post"]));
		}
	} else {
		http_response_code(400);
	}
} else {
	http_response_code(403);
}

?>