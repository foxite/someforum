<?php

if (isset($_POST["title"]) && isset($_POST["content"])) {
	require_once("../includes/session.php");

	if (!empty($_SESSION["userid"])) {
		// Validate content
		if (sizeof($_POST["title"]) > 128) {
			die("Error: that title is too long.");
		}
		if (sizeof($_POST["content"]) > 2000) {
			die("Error: the content is too long.");
		}

		// Sanitize content
		require_once("../includes/dbconnect.php");

		$title = $conn->real_escape_string(htmlspecialchars($string = preg_replace('/[\x00-\x1F\x7F]/u', '', $_POST["title"])));
		
		require_once("../includes/parsedown.php");
		$parsedown = new Parsedown();
		$parsedown->setSafeMode(true);
		$content = $conn->real_escape_string($parsedown->text($_POST["content"]));
		$markdownSource = $conn->real_escape_string($_POST["content"]);
		
		$sql = "INSERT INTO threads (author_id, title, content, posted, markdown_source)
		VALUES ({$_SESSION['userid']}, '{$title}', '{$content}', now(), '{$markdownSource}')";

		if ($conn->query($sql) === true) {
			addNotifToSession(new Notification("white", "limegreen", "Your post has been made", null, Notification::NotifShowOnce));
			http_response_code(200);
			header("Location: ../viewpost.php?post=" . $conn->insert_id);
		} else {
			addNotifToSession(new Notification("white", "red", "Your post could not be made", null, Notification::NotifShowOnce));
			http_response_code(500);
			header("Location: ../newpost.php");
		}
	} else {
		http_response_code(401);
		echo("Error: You are not logged in.");
	}
} else {
	http_response_code(400);
}

?>