<?php

require("../includes/session.php");

if (empty($_SESSION["userid"])) {
	echo("Error: You are not logged in.");
	http_response_code(409);
} else {
	unset($_SESSION["userid"]);
	require_once("../includes/notification.php");
	addNotifToSession(new Notification("white", "limegreen", "You are now logged out", null, Notification::NotifShowOnce));
	http_response_code(200);
	header("Location: ../index.php"); // Redirect by HTTP: from https://stackoverflow.com/a/768472/3141917
}

?>