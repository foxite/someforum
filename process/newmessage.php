<?php

include("../includes/session.php");

if (!empty($_SESSION["userid"])) {
	if (!empty($_POST["recipient"]) && !empty($_POST["subject"]) && sizeof($_POST["subject"]) <= 128 && !empty($_POST["message"]) && sizeof($_POST["message"]) <= 2000) {
		// Check username
		require_once("../includes/dbconnect.php");

		$recipient = $conn->real_escape_string($_POST["recipient"]);

		$sql = "SELECT id FROM users
		WHERE username = '{$recipient}'
		LIMIT 1";

		$result = $conn->query($sql);

		if ($result->num_rows == 1) {
			// Send message
			$row = $result->fetch_assoc();

			// Sanitize text: Remove control characters, html encode, sql escape.
			$subject = $conn->real_escape_string(htmlspecialchars($string = preg_replace('/[\x00-\x1F\x7F]/u', '', $_POST["subject"])));
			
			// Parsedown will escape HTML for us if we call `setSafeMode(true)`.
			require_once("../includes/parsedown.php");
			$parsedown = new Parsedown();
			$parsedown->setSafeMode(true);
			$message = $conn->real_escape_string($parsedown->text($_POST["message"]));
			
			$sql = "INSERT INTO messages (sender_id, recipient_id, subject, message, read_by_recipient)
			VALUES ({$_SESSION["userid"]}, {$row["id"]}, '{$subject}', '{$message}', FALSE)";
			
			if ($conn->query($sql)) {
				addNotifToSession(new Notification("white", "limegreen", "Your message has been sent", null, Notification::NotifShowOnce));
				http_response_code(200);
				header("Location: ../messages.php?show=sent");
			} else {
				addNotifToSession(new Notification("white", "red", "Your message could not be sent", null, Notification::NotifShowOnce));
				http_response_code(500);
				header("Location: ../composepm.php?recipient=" . urlencode($_POST["recipient"]) . "&subject=" . urlencode($_POST["subject"]) . "&message=" . urlencode($_POST["message"]));
			}
		} else {
			addNotifToSession(new Notification("white", "red", "That user was not found", null, Notification::NotifShowOnce));
			http_response_code(404);
			header("Location: ../composepm.php?recipient=" . urlencode($_POST["recipient"]) . "&subject=" . urlencode($_POST["subject"]) . "&message=" . urlencode($_POST["message"]));
		}
	} else {
		http_response_code(400);
	}
} else {
	http_response_code(403);
}

?>