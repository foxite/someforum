<?php
// TODO: processlogin.php and processregister.php should be requested using javascript from login.php, and they should return JSON which the client uses to determine
//  if the login was successful, and redirect to the homepage.

require_once("../includes/config.php");

// Check if we have the data we need. Otherwise, give a HTTP 400 (Bad request).
if (isset($_POST["username"]) && isset($_POST["password"])) {
	require("../includes/session.php");
	if (empty($_SESSION["userid"])) {
		// Connect to the DB. This script creates the mysqli connection $conn.
		require_once("../includes/dbconnect.php");

		// Check if a user with the given name exists in the database.
		$username = $conn->real_escape_string($_POST["username"]); // It is important to sanitize strings passed into SQL queries.
		$sql = "SELECT id, password FROM users
		WHERE username = '{$username}'
		LIMIT 1";
		$result = $conn->query($sql);

		// If we find one record, that means we have a user. If we find 0, then that user does not exist.
		if ($result->num_rows == 1) {
			// Since we only have one record, fetch_assoc() does not need to be called in a loop.
			$record = $result->fetch_assoc();
			
			if (password_verify($_POST["password"], $record["password"])) { // Use password_verify to check if password matches a hash generated with password_hash.
				// Log in user using PHP sessions.
				require_once("../includes/notification.php");
				
				$_SESSION["userid"] = $record["id"];
				
				addNotifToSession(new Notification("white", "limegreen", "You are now logged in", null, Notification::NotifShowOnce));
				
				http_response_code(200);
				header("Location: ../index.php"); // Redirect by HTTP: from https://stackoverflow.com/a/768472/3141917
			} else {
				http_response_code(401);
				//header("WWW-Authenticate: "); // This is officially required ("MUST"), but browsers seem to handle it pretty well when it's not there.
				echo("Username or password is invalid.");
			}
		} else {
			http_response_code(401);
			echo("Username or password is invalid.");
		}
	} else {
		http_response_code(400);
		echo("You are already logged in.");
	}
} else {
	http_response_code(400); // Bad request
}

?>