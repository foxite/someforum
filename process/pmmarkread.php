<?php

require("../includes/session.php");
if (!empty($_SESSION["userid"])) {
	if (!empty($_POST["id"]) && is_numeric($_POST["id"]) && intval($_POST["id"]) > 0) {
		$messageId = intval($_POST["id"]);

		$sql = "UPDATE messages
		SET read_by_recipient = TRUE
		WHERE id = {$messageId}";

		require_once("../includes/dbconnect.php");
		$conn->query($sql);
		if ($conn->affected_rows == 1) {
			http_response_code(204); // No content; successful but no response content is needed/wanted
		} else if ($conn->affected_rows == 0) {
			http_response_code(404);
		}
	} else {
		http_response_code(400);
	}
} else {
	http_response_code(403);
}

?>