<?php
// Include the page header (and footer) that's used on all pages.
// It includes all HTML needed to make a functional page, as well as our <nav> and potentially CSS and JS, and calls start_session if it wasn't already called.
// This allows us to only write the HTML that we care about, and it makes updating the global layout much easier, as we only have to edit one page.
// It also uses $pagetitle, if it is set, and uses it in the <title> tag.
$pagetitle = "Home";
include("includes/pageheader.php");

$startingFrom = 0;
$count = 25;
if (isset($_GET["from"]) && is_numeric($_GET["from"]) && intval($_GET["from"]) >= 0) {
	$startingFrom = intval($_GET["from"]);
}
if (isset($_GET["count"]) && is_numeric($_GET["count"]) && intval($_GET["count"]) >= 1 && intval($_GET["count"]) <= 100) {
	$count = intval($_GET["count"]);
}

include("includes/listing.php");

include("includes/pagefooter.php");
?>