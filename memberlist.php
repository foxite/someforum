<?php
$pagetitle = "Members";
include("includes/pageheader-prehead.php");
?>
<link rel="stylesheet" href="css/table.css" />
<?php
include("includes/pageheader-posthead.php");

function sortParams($column) {
	$ret = "sort=" . $column;

	if (!isset($_GET["des"])) {
		$ret .= "&des=1";
	}
	return $ret;
}

?>

<h2>Member list</h2>
<table style="table-layout: auto; width: 100%">
	<tr>
		<th><a href="memberlist.php?<?php echo(sortParams("username")); ?>">Username</a></th>
		<th><a href="memberlist.php?<?php echo(sortParams("registered")); ?>">Registered</a></th>
		<th><a href="memberlist.php?<?php echo(sortParams("fruitname")); ?>">Fav fruit</a></th>
	</tr>
<?php
require_once("includes/dbconnect.php");

$sql = "SELECT u.id, u.username, u.registered, u.fav_fruit, f.variety AS fruitvariety, ft.name AS fruitname FROM users AS u
LEFT JOIN fruit AS f ON (u.fav_fruit = f.id)
LEFT JOIN fruit_types AS ft ON (f.fruit_type_id = ft.id)";

if (isset($_GET["sort"]) && ($_GET["sort"] == "username" || $_GET["sort"] == "registered" || $_GET["sort"] == "fruitname")) {
	$sql .= "
	ORDER BY " . $conn->real_escape_string($_GET["sort"]);
	if (isset($_GET["des"]) && $_GET["des"] == "1") {
		$sql .= " DESC";
	}
}

$result = $conn->query($sql);

if ($result->num_rows > 0) {
	while ($row = $result->fetch_assoc()) {
		echo("<tr><td><a href='profile.php?user={$row['id']}'>{$row['username']}</a></td><td>{$row['registered']}</td><td>");
		if (!empty($row['fav_fruit'])) {
			echo("{$row['fruitvariety']} {$row['fruitname']}");
		}
		echo("</td></tr>");
	}
} else {
	echo("No users found");
}
?>
</table>
<?php
include("includes/pagefooter.php");
?>