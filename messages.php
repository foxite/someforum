<?php

require("includes/session.php");

if (empty($_SESSION["userid"])) {
	$pagetitle = "Error";
	require("includes/pageheader.php");
	echo("You need to be logged in.");
} else {
	$pagetitle = "Messages";
	require("includes/pageheader-prehead.php");
	?><link rel="stylesheet" href="css/pm.css" />
	<link rel="stylesheet" href="css/markdown.css" />
	<script type="text/javascript" src="js/pm.js"></script>
	<?php
	require("includes/pageheader-posthead.php");
	?>

	<a href="composepm.php">New message...</a>
	
	<div id="messageList">
	<h2>Messages</h2>
	<div id="messages"><?php
		// Fetch messages
		$sql = "SELECT m.id, m.sender_id, m.subject, m.message, m.read_by_recipient, m.sent, u.username, u.id AS senderid FROM messages AS m
		LEFT JOIN users AS u ON (m.sender_id = u.id)
		WHERE m.recipient_id = {$_SESSION["userid"]}
		ORDER BY m.read_by_recipient DESC, m.sent DESC"; // Should cause SQL to sort messages with the newest ones first, and have all unread ones on top
		
		require_once("includes/dbconnect.php");
		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
			while ($row = $result->fetch_assoc()) {
				?><div id="message-<?php echo($row["id"]) ?>" data-pmid="<?php echo($row["id"]); ?>" class="message <?php echo($row["read_by_recipient"] ? "read" : "unread"); ?>">
					<div class="messageheader">From <a class="sender" href="viewprofile?user=<?php echo($row["senderid"]); ?>"><?php echo($row["username"]); ?></a> on <time><?php
					echo($row["sent"]); ?></time>: <span class="subject"><?php echo($row["subject"]); ?></span></div>
					<div class="messagebody"><?php echo($row["message"]); ?></div>
					<div class="messagefooter"><a class="button" href="composepm.php?recipient=<?php echo($row["username"]); ?>" class="footerbutton">Reply</a> <?php
					if (!$row["read_by_recipient"]) {
						?><a class="button" onclick="markAsRead(this);">Mark as read</a><?php
					}
					?></div>
				</div><?php
			}
		} else {
			echo("You have no messages.");
		}
		//   Output message
	?></div></div>
	<?php
}

require("includes/pagefooter.php");

?>