<?php

require("includes/session.php");

if (empty($_SESSION["userid"])) {
	$pagetitle = "Error";
	include("includes/pageheader.php");
	echo("You need to be logged in.");
} else {
	$pagetitle = "Compose PM";
	include("includes/pageheader-prehead.php");
	?>
	<script type="text/javascript" src="js/pm.js"></script>
	<?php
	include("includes/pageheader-posthead.php");
	?>
	<form id="pmform" action="process/newmessage.php" method="POST">
	<fieldset>
		<legend>Message</legend>
		<label for="recipient">Recipient:</label><br />
		<input type="text" id="recipient" name="recipient" maxlength="20" <?php if (isset($_GET["recipient"])) { echo("value='{$_GET["recipient"]} '"); } ?> required /><br />
		<label for="subject">Subject:</label><br />
		<input type="text" id="subject" name="subject" size="80" maxlength="128" <?php if (isset($_GET["subject"])) { echo("value='{$_GET["subject"]} '"); } ?> required /><br />
		<label for="message">Message:</label><br />
		<textarea id="message" name="message" rows="10" cols="80" maxlength="2000" required><?php if (isset($_GET["message"])) { echo("{$_GET["message"]}"); } ?></textarea>
		<br />
		<input type="submit" value="Send" /> <button type="button" onclick="getPmUrl()">Get URL with this template</button><br />
		<input type="text" id="templateUrl" readonly style="display:none;" />
	</fieldset>
	</form>
	<?php
}

include("includes/pagefooter.php");
?>